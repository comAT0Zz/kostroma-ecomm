import postgresql as ps


# from werkzeug import check_password_hash, generate_password_hash


class Configuration(object):
    DEBUG = True


class MetaSingleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(MetaSingleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Database(metaclass=MetaSingleton):
    connection = None

    def connect(self):
        conn = 'shopadmin:shopadmin@shop.cn5r3kwusnqh.us-east-1.rds.amazonaws.com:5432/shopdb'
        #conn = 'postgres:postgres@localhost:5432/shopdb'
        if self.connection is None:
            self.connection = ps.open(conn)

        return self.connection





