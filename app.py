from flask import Flask
from cfg import Configuration


from blueprints.home_page.home_page_route import home_page_bp
from blueprints.sign_up.__init__ import sign_up_bp
from blueprints.sign_in.__init__ import sign_in_bp
from blueprints.catalog.catalog_route import catalog_bp
from blueprints.personal_account.__init__ import personal_account_bp
from blueprints.search.__init__ import search_bp


app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
# app.config.from_object(Configuration)

# db = pg.open('postgres:postgres@localhost:5432/shopdb')


app.register_blueprint(home_page_bp, url_prefix='/')
app.register_blueprint(sign_up_bp, url_prefix='/sign_up')
app.register_blueprint(sign_in_bp, url_prefix='/sign_in')
app.register_blueprint(catalog_bp, url_prefix='/catalog')
app.register_blueprint(personal_account_bp, url_prefix='/personal_account')
app.register_blueprint(search_bp, url_prefix='/search')
