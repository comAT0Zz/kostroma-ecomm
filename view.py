from app import app
from flask import render_template, redirect, url_for, make_response, request, session
from db_queries import select_user


class User():
    def __init__(self, id, login, name, phone, email, address):
        self.id = id
        self.login = login
        self.name = name
        self.phone = phone
        self.email = email
        self.address = address

    def get_login(self):
        return self.login

    def get_id(self):
        return self.id

    def get_name(self):
        return self.name

    def get_phone(self):
        return self.phone

    def get_email(self):
        return self.email

    def get_address(self):
        return self.address




@app.route('/delete-user/')
def delete_user():
    session.pop(User.name, None)
    return 'User deleted'


@app.route('/', methods = ['post', 'get'])
def login():
    global user
    email = request.form['email']
    pswd = request.form['password']
    if request.method == 'post':
        data = select_user(email, pswd)
        user = User(id=data[0], login=data[1], name=data[2], phone=data[3], email=data[4], address=data[5])
        if user in session:
            session[user] = session.get(user)
        else:
            session[user] = user
        render_template('personal_account.html', n=user.name, l=user.login)
    # return render_template('sign_in.html', n=user.fisrt_name)
    return redirect(url_for('personal_account_bp.personal_account'))


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html')

@app.route('/contacts', methods = ["POST", "GET"])
def contacts():

    return render_template('contact.html')


if __name__ == "__main__":
    app.run(debug=True)
