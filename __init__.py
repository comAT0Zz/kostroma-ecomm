from app import app
from flask import render_template

import view

if __name__ == "__main__":
    app.run(host='127.0.0.1', port='8000', debug=True)
