from . import db_queries as dbq


class GoodsType:
    def __init__(self, type_id, type_name, type_url):
        self.type_id = type_id
        self.type_name = type_name
        self.type_url = type_url


class GoodsCategory:
    def __init__(self, category_id, category_name, category_url):
        self.category_id = category_id
        self.category_name = category_name
        self.category_url = category_url
        self.child_types = []

    def add_child_type(self, child_type: GoodsType):
        self.child_types.append(child_type)


class CategoryList:
    def __init__(self):
        self.categories = []

    def __getitem__(self, item):
        return self.categories[item]

    def create_list(self):
        categories = dbq.select_list_of_categories()
        types = dbq.select_list_of_types()
        for cat in categories:
            goods_category = GoodsCategory(category_id=cat['goods_category_id'],
                                           category_name=cat['category_name'],
                                           category_url=cat['category_url'])
            self.categories.append(goods_category)
            for tp in filter(lambda x: x['goods_category_id'] == goods_category.category_id, types):
                goods_type = GoodsType(type_id=tp['goods_type_id'],
                                       type_name=tp['type_name'],
                                       type_url=tp['type_url'])
                goods_category.child_types.append(goods_type)


class Brand:
    def __init__(self, brand_id, brand_name, brand_url):
        self.brand_id = brand_id
        self.brand_name = brand_name
        self.brand_url = brand_url


class BrandList:
    def __init__(self):
        self.brands = []

    def __getitem__(self, item):
        return self.brands[item]

    def create_list(self):
        brands = dbq.select_list_of_brands()
        for brand in brands:
            self.brands.append(Brand(brand_id=brand['brand_id'],
                                     brand_name=brand['brand_name'],
                                     brand_url=brand['brand_url']))


class Group:
    def __init__(self, group_id, group_name, group_description):
        self.group_id = group_id
        self.group_name = group_name
        self.group_description = group_description

    def get_product_list(self):
        assert False


class GroupCategory(Group):
    def __init__(self, category_id):
        category = dbq.select_category(category_id)
        super().__init__(group_id=category_id,
                         group_name=category['category_name'],
                         group_description=category['category_discription'])

    def get_product_list(self, page):
        return dbq.select_list_of_goods_by_category(self.group_id, page)


class GroupType(Group):
    def __init__(self, type_id):
        gtype = dbq.select_type(type_id)
        super().__init__(group_id=type_id,
                         group_name=gtype['type_name'],
                         group_description=gtype['type_discription'])

    def get_product_list(self, page):
        return dbq.select_list_of_goods_by_type(self.group_id, page)


class GroupBrand(Group):
    def __init__(self, brand_id):
        brand = dbq.select_brand(brand_id)
        super().__init__(group_id=brand_id,
                         group_name=brand['brand_name'],
                         group_description=brand['brand_description'])

    def get_product_list(self, page):
        return dbq.select_list_of_goods_by_brand(self.group_id, page)


class Product:
    def __init__(self, product_id, product_name, product_url, image_link, product_price):
        self.product_id = product_id
        self.product_name = product_name
        self.product_url = product_url
        self.image_link = image_link
        self.product_price = product_price




class ProductList:
    def __init__(self, group, page):
        self.products = []
        products = group.get_product_list(page)
        for product in products:
            self.products.append(Product(product_id=product['goods_id'],
                                         product_name=product['goods_name'],
                                         product_url=product['goods_url'],
                                         image_link=product['image_link'],
                                         product_price=product['goods_price']))

    def __getitem__(self, item):
        return self.products[item]





# if __name__ == "__main__":
    # categs = CategoryList()
    # categs.create_list()
    # for cat in categs:
    #     print(cat.category_id, cat.category_name, cat.child_types, sep='\n')
