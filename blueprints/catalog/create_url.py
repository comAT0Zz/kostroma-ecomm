from transliterate import translit, get_available_language_codes
from re import sub


def create_url(name, id):
    result = translit(name, 'ru', reversed=True)
    result = sub(r' |\'', '-', result)
    result = result + '-' + str(id)
    result = result.lower()
    return result


if __name__ == "__main__":
    print(create_url('Название товара \' Product', 132))



