from cfg import Database

db = Database().connect()


# def select_categories():
#     categories = db.query('''select gt.goods_type_id, gt.type_name, gt.goods_category_id, gc.category_name
#                              from goods_type as gt
#                              join goods_category as gc
#                              on gt.goods_category_id = gc.goods_category_id''')
#     return categories


def select_list_of_categories():
    categories = db.query('select goods_category_id, category_name, category_url from goods_category')
    return categories


def select_list_of_types():
    types = db.query('select goods_type_id, type_name, goods_category_id, type_url from goods_type order by type_name')
    return types

def select_list_of_types_by_category(category_id):
    types = db.query(f'select goods_type_id, type_name, goods_category_id, type_url from goods_type where goods_category_id = {category_id} order by type_name')
    return types

def select_list_of_brands():
    brands = db.query('select brand_id, brand_name, brand_url from brand order by brand_name ')
    return brands

def select_list_of_brands_by_category(category_id):
    brands = db.query(f'''select b.brand_id, brand_name, brand_url from brand as b
                        join goods as g on g.brand_id = b.brand_id
                        join goods_type as gt on gt.goods_type_id = g.goods_type_id where goods_category_id = {category_id} ''')
    uno = []
    for brand in brands:
        if brand not in uno:
            uno.append(brand)

    return uno

def select_list_of_goods():
    goods = db.query('select*from goods')
    return goods

def select_type(type_id):
    gtype = db.query(f'select type_name, type_discription from goods_type where goods_type_id = {type_id} limit 1')
    return gtype[0]


def select_category(category_id):
    category = db.query(f'select category_name, category_discription from goods_category where goods_category_id = {category_id} limit 1')
    return category[0]


def select_brand(brand_id):
    brand = db.query(f'select brand_name, brand_logo_link, brand_description from brand where brand_id = {brand_id} limit 1')
    return brand[0]


def select_goods(goods_id):
    goods = db.query(f'''select*from goods 
                     join goods_type as gt on gt.goods_type_id = goods.goods_type_id
                     join goods_category as gc on gc.goods_category_id = gt.goods_category_id where goods_id = {goods_id} ''')
    return goods[0]


def select_list_of_goods_by_type(type_id, page):
    page = int(page)
    if page < 1:
        page = 1
    products = db.query(f'''select goods_id, goods_name, goods_url, image_link, goods_price from goods
                            where goods_type_id={type_id} 
                            order by goods_price, goods_id
                            limit 21 offset {(page - 1) * 21}''')
    return products


def select_list_of_goods_by_category(category_id, page):
    if page < 1:
        page = 1

    products = db.query(f'''select goods_id, goods_name, goods_url, image_link, goods_price from goods 
                            join goods_type as gt on goods.goods_type_id = gt.goods_type_id
                            join goods_category as gc on gt.goods_category_id = gc.goods_category_id
                            where gc.goods_category_id = {category_id}
                            order by goods_price, goods_id
                            limit 21 offset {(page - 1) * 21}''')
    return products


def select_list_of_goods_by_brand(brand_id, page):
    if page < 1:
        page = 1

    products = db.query(f'''select goods_id, goods_name, goods_url, image_link, goods_price from goods where brand_id = {brand_id}
                            order by goods_price, goods_id
                            limit 21 offset {(page - 1) * 21}''')
    return products

def last_page(category_id):
    lastpage = len(db.query(f'''select goods_id from goods join goods_type as gt on goods.goods_type_id = gt.goods_type_id
                            join goods_category as gc on gt.goods_category_id = gc.goods_category_id
                            where gc.goods_category_id = {category_id} '''))//21+1
    return lastpage

def list_of_country(category_id):
    products = db.query(f'''select goods_country from goods 
                                join goods_type as gt on goods.goods_type_id = gt.goods_type_id
                                join goods_category as gc on gt.goods_category_id = gc.goods_category_id
                                where gc.goods_category_id = {category_id}''')
    country = []
    for j in products:
        if j not in country:
            country.append(j)

    return sorted(country)


def filter_list_of_goods_by_type(type_name, category_id):

    products = db.query(f'''select goods_id, goods_name, goods_url, image_link, goods_price from goods 
                            join goods_type as gt on goods.goods_type_id = gt.goods_type_id
                            where gt.type_name={type_name} and gt.goods_category_id = {category_id} ''')
    return products

def filter_list_of_goods_by_brand(brand_name, category_id):

    products = db.query(f'''select goods_id, goods_name, goods_url, image_link, goods_price from goods as g
                            join brand as b on g.brand_id = b.brand_id
                            join goods_type as gt on g.goods_type_id = gt.goods_type_id
                            where b.brand_name={brand_name} and gt.goods_category_id = {category_id} ''')
    return products

def filter_list_of_goods_by_country(country_name, category_id):

    products = db.query(f'''select goods_id, goods_name, goods_url, image_link, goods_price from goods as g
                            join goods_type as gt on g.goods_type_id = gt.goods_type_id
                            where goods_country={country_name} and gt.goods_category_id = {category_id} ''')
    return products

# if __name__ == '__main__':
    # # category list test
    # cat_list = select_list_of_categories()
    # type_list = select_list_of_types()
    # for cat in cat_list:
    #     print()
    #     for tp in filter(lambda x: x['goods_category_id'] == cat['goods_category_id'], type_list):
    #         print(tp)
    #
    # # product list test
    # prod_list = select_list_of_goods_by_type(2, 1)
    # print(prod_list)
    # prod_list = select_list_of_goods_by_category(1, 1)
    # print(prod_list)
    #
    # # category_test
    # print(select_category(1))
    #
    # # type_test
    # print(select_type(1))

