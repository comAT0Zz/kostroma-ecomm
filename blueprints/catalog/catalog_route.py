from flask import Blueprint, render_template, abort, request
from re import split
from postgresql.exceptions import UndefinedColumnError
from . import catalog_models as models
from . import db_queries as dbq
import random


catalog_bp = Blueprint('catalog_bp', __name__, template_folder='templates', static_folder='static')


@catalog_bp.route('/categories')
def catalog():
    categories = models.CategoryList()
    categories.create_list()
    return render_template('categories.html', categories=categories)


@catalog_bp.route('/brands',methods=["POST", "GET"])
def brand_catalog():
    brands = models.BrandList()
    brands.create_list()
    return render_template('brands.html', brands=brands)


def goods_list(group_url, page, group):
    group_id = split(r'-', group_url)[-1]
    try:
        group = group(group_id)
    except UndefinedColumnError:
        abort(404)
    except KeyError:
        abort(404)
    product_list = models.ProductList(group, page)
    lastpage = dbq.last_page(group_id)
    brandlist = dbq.select_list_of_brands_by_category(group_id)
    typelist = dbq.select_list_of_types_by_category(group_id)
    countrylist = dbq.list_of_country(group_id)
    if page > lastpage or page < 1 :
        return abort(404)
    return render_template('goods_list.html', group=group, product_list=product_list, page=page, GroupCategory=group_id,
                           lastpage=lastpage, brandlist=brandlist, typelist=typelist, countrylist=countrylist)


@catalog_bp.route('/<category_url>/<type_url>/<int:page>')
def goods_list_by_type(type_url, category_url, page):
    return goods_list(type_url, page, models.GroupType)


@catalog_bp.route('/<category_url>/<int:page>', methods=["POST", "GET"])
def goods_list_by_category(category_url, page):
    if request.method == "POST":
        typeselect = request.form.getlist("typeselect")
        brandselect = request.form.getlist("brandselect")
        countryselect = request.form.getlist("countryselect")
        category = dbq.select_category(category_url)
        typelist = []
        if typeselect:
            for type in typeselect:
                typelist.extend(dbq.filter_list_of_goods_by_type(f"'{type}'", category_url))
        if brandselect:
            for brand in brandselect:
                uno = dbq.filter_list_of_goods_by_brand(f"'{brand}'", category_url)
                for j in uno:
                    if j not in typelist:
                        typelist.append(j)
        if countryselect:
            for country in countryselect:
                uno = dbq.filter_list_of_goods_by_country(f"'{country}'", category_url)
                for j in uno:
                    if j not in typelist:
                        typelist.append(j)

        return render_template('categories.html', product_list=typelist, category=category, category_id=category_url)
    return goods_list(category_url, page, models.GroupCategory)


@catalog_bp.route('/brand/<brand_url>/<int:page>')
def goods_list_by_brand(brand_url, page):
    return goods_list(brand_url, page, models.GroupBrand)


@catalog_bp.route('/<goods_url>')
def goods(goods_url):
    goods_id = split('-', goods_url)[-1]
    product = dbq.select_goods(goods_id)
    random_beer = random.sample(dbq.select_list_of_goods(), 3)

    return render_template('beer.html', product=product, random_beer=random_beer)


