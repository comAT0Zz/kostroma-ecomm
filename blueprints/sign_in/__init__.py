from flask import Blueprint, render_template

sign_in_bp = Blueprint('sign_in_bp', __name__, template_folder='templates', static_folder='static')


@sign_in_bp.route('/')
def sign_in():
    return render_template('sign_in.html')
