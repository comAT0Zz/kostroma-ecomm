from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired
from flask import render_template, redirect, url_for, make_response, request, session
from app import routes


class LoginForm(FlaskForm):
    username = StringField('Логин', validators=[DataRequired()])
    password = PasswordField('Пароль', validators=[DataRequired()])
    remember_me = BooleanField('Запомнить')
    submit = SubmitField('Войти')


username = request.form.get('username')
password = request.form.get('password')


@app.route('/unset')
def unset():
    session.pop(LoginForm.username, None)
    return redirect(url_for('home_page_bp.home_page'))


@app.route('/login/', methods=['POST', 'GET'])
def login():
    message = ''
    if request.method == 'POST':
        if username == 'User@mail.com' and password == '123':
            session[username] = request.form[username]
        return redirect(url_for('home_page_bp.home_page'))
    else:
        message = 'Try again'
        return render_template('sign_in.html', message=message)


@app.route('/logout/')
def logout():
    session.pop(username, None)
    return 'Deleted'
