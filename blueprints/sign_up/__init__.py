from flask import Blueprint, render_template

sign_up_bp = Blueprint('sign_up_bp', __name__, template_folder='templates', static_folder='static')

@sign_up_bp.route('/')
def sign_up():
    return render_template('sign_up.html')
