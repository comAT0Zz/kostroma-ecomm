from flask import Blueprint, render_template, redirect, url_for
from . import home_page_models as models
from . import db_queries as db
from re import split
home_page_bp = Blueprint('home_page_bp', __name__, template_folder='templates', static_folder='static')


@home_page_bp.route('/')
def home_page():
    products = models.Goods()
    products.create_list()
    return render_template('home_page.html', products=products)




