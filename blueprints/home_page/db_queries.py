from cfg import Database

db = Database().connect()


def select_goods(limit):
    limit = int(limit)
    goods = db.query(f'''SELECT goods_id, goods_name, goods_price, goods_url, image_link, goods_size,
                         goods_amount, goods_strength, goods_country, goods_discription FROM goods
                         ORDER BY random()
                         limit {limit}''')
    return goods

def select_product_by_id(goods_id):
    product= db.query(f'''SELECT goods_name, goods_price, goods_url, image_link, goods_size, goods_amount,
                          goods_strength, goods_country, goods_discription  FROM goods 
                          WHERE goods_id = {goods_id} GROUP BY goods_id''')
    return product[0]