from . import db_queries as db


class GoodsGrouped:
    def __init__(self, goods_id, goods_name, goods_price, goods_url, image_link, goods_size, goods_amount,
                 goods_strength, goods_country, goods_discription):
        self.goods_id = goods_id
        self.goods_name = goods_name
        self.goods_price = goods_price
        self.goods_url = goods_url
        self.image_link = image_link
        self.goods_size = goods_size
        self.goods_amount = goods_amount
        self.goods_strength = goods_strength
        self.goods_country = goods_country
        self.goods_discription = goods_discription


class Goods:
    def __init__(self):
        self.goods = []

    def __getitem__(self, item):
        return self.goods[item]

    def create_list(self):
        goods_list = db.select_goods(limit=20)
        for i in goods_list:
            group = GoodsGrouped(goods_id=i['goods_id'],
                                 goods_name=i['goods_name'],
                                 goods_price=i['goods_price'],
                                 goods_url=i['goods_url'],
                                 image_link=i['image_link'],
                                 goods_size=i['goods_size'],
                                 goods_amount=i['goods_amount'],
                                 goods_strength=i['goods_strength'],
                                 goods_country=i['goods_country'],
                                 goods_discription=i['goods_discription'])
            self.goods.append(group)
