from flask import Blueprint, render_template

personal_account_bp = Blueprint('personal_account_bp', __name__, static_folder='static', template_folder='templates')


@personal_account_bp.route('/')
def personal_account():
    return render_template('personal_account.html')
