from flask import Blueprint, render_template

search_bp = Blueprint('search_bp', __name__, static_folder='static', template_folder='templates')


@search_bp.route('/')
def search():
    return render_template('search.html')
